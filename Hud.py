"""
    :Mainwork Arlt Robin:
"""

import pygame


class Hud:

    def __init__(self):
        self.height = 600
        self.width = 200
        self.bar_vertical_padding = 20

    def draw(self, surface, player_hp, player_currency):
        """
            Draws a menu bar with given arguments as Lives and Gold on given surface
        """
        
        font = pygame.font.Font(None, 25)
        hp_bar = font.render(f'Lives: {player_hp}', True, (200, 200, 200))
        currency_bar = font.render(f'Gold: {player_currency}', True, (200, 200, 200))

        pygame.draw.rect(surface, (150, 150, 150), (800, 0, self.width, self.height))
        pygame.draw.rect(surface, (0, 0, 0), (800, 0, self.width, self.height), 1)
        surface.blit(hp_bar, (855, 15))
        surface.blit(currency_bar, (855, 15 + hp_bar.get_height() + self.bar_vertical_padding))
