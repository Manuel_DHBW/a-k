"""
    :Main work Manuel Klaiber:
"""

import pygame
from copy import deepcopy


class Config:
    """Provides project variables."""
    
    DEBUG = False
    DIMENSIONS = 1000, 600
    TILE_SIZE = 40
    TITLE = 'A&K - SALAD TD_old'
    HIGHLIGHT_BORDER_WIDTH = 2.5
    SPAWN_SPEED_MS = 500
    FPS = 60
    
    COLOUR_HOVER = (220, 160, 120)  # orange
    COLOUR_CLICK = (240, 120, 160)  # red
    COLOUR_SHOT_RANGE = (0, 200, 0) # green
    COLOUR_HITBOX = (250, 0, 250)   # pink
    
    # Level specific
    
    # level_tile : colour
    DEBUG_SPRITES = {
        -1: (255,   0,   0), # red (not found)
         0: ( 99,  66,  33), # brown (path)
         1: (240, 240, 240), # whitish (base)
         2: (120, 120, 120), # grey (building space)
         3: (220, 220,  60), # yellow (blocked space)
    }
    # 0 = path
    # 1 = base
    # 2 = free space
    # 3 = blocked space
    LEVEL1 = [
        [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 0, 0, 0, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 0, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 0, 0, 0, 0, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 2, 2, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 0, 2, 2, 2, 2, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 2, 3, 3, 3, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 0, 0, 0, 1, 3, 3, 3, 3, 3, 3],
        [2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3],
    ]
    PATH_LEVEL1 = ( # update so that real tile e.g.(0, 1) can be given
        (  0.5 * TILE_SIZE,  1.5 * TILE_SIZE),
        (  5.5 * TILE_SIZE,  1.5 * TILE_SIZE),
        (  5.5 * TILE_SIZE,  7.5 * TILE_SIZE),
        (  2.5 * TILE_SIZE,  7.5 * TILE_SIZE),
        (  2.5 * TILE_SIZE, 11.5 * TILE_SIZE),
        ( 11.5 * TILE_SIZE, 11.5 * TILE_SIZE),
        ( 11.5 * TILE_SIZE,  4.5 * TILE_SIZE),
        ( 15.5 * TILE_SIZE,  4.5 * TILE_SIZE),
        ( 15.5 * TILE_SIZE, 13.5 * TILE_SIZE),
        ( 18.5 * TILE_SIZE, 13.5 * TILE_SIZE),
    )
    WAVES_LEVEL1 = [
        [3, 2, 4, 1],
        [3, 3, 3, 3],
    ]
    
    IMAGES = {}
    ANIMATIONS = {}

    @staticmethod
    def get_level():
        return deepcopy(Config().LEVEL1)

    @staticmethod
    def setup():
        """Load assets and make available."""
        
        def load_static_images():
            Config().IMAGES['Level1'] = pygame.image.load('assets/backgrounds/level1.png')
        
        def populate_animations():
            """Loades frames of consecutive cycles and bundles them together itno a dictionary."""

            def load_animation(path, cnt):
                """
                    Loads images of given path and returns them as list.
                    XXX is replaced by the current image number filled with leading zeros.
                """
                
                return [pygame.image.load(path.replace('XXX', f'{x :03}')) for x in range(1, cnt)]

            def crop_animation(animation, sides = 0, top = 0, bottom = 0):
                """Crop boundaries of all images in a list and return the list."""
                
                w, h = animation[0].get_rect()[-2:]
                return [image.subsurface((sides, top, w - 2 * sides, h - top - bottom)) for image in animation]

            def scale_animation(animation, factor = 1):
                """Resizes all images in a list and return the list. (Maintaining aspect ratio)"""

                def scale_by_factor(img, factor :(int, float)):
                    w, h = img.get_rect()[-2:]
                    return pygame.transform.scale(img, (int(w * factor), int(h * factor)))

                return [scale_by_factor(image, factor) for image in animation]

            def convert_animation(animation):
                """Converts all images in a list (keeping transparency) and enumerate them in a dictionary."""
                return {i: image.convert_alpha() for i, image in enumerate(animation)}
                
            #load
            goblin_walk = load_animation('assets/units/goblins/PNG/Goblin_1/PNG Sequences/Walking/0_Goblin_Walking_XXX.png', 24)
            wraith_walk = load_animation('assets/units/wraiths/PNG/Wraith_01/PNG Sequences/Walking/Wraith_01_Moving Forward_XXX.png', 12)
            golem_walk = load_animation('assets/units/golems/PNG/Golem_6/PNG Sequences/Walking/Golem_03_Walking_XXX.png', 18)
            satyr_walk = load_animation('assets/units/satyrs/PNG/Satyr_03/PNG Sequences/Walking/Satyr_03_Walking_XXX.png', 18)
            # wraith_attack = load_animation('assets/units/wraiths/PNG/Wraith_01/PNG Sequences/Walking/Wraith_01_Moving Forward_XXX.png', 12)
            # wraith_death = load_animation('assets/units/wraiths/PNG/Wraith_01/PNG Sequences/Dying/Wraith_01_Dying_XXX.png', 14)

            #crop TODO
            goblin_walk = crop_animation(goblin_walk)
            wraith_walk = crop_animation(wraith_walk)
            golem_walk = crop_animation(golem_walk, 50, 75, 0)
            satyr_walk = crop_animation(satyr_walk, 70, 30, 70)
            # attack = crop_animation(attack)
            # death = crop_animation(death)

            # scale TODO
            goblin_walk = scale_animation(goblin_walk, 0.08)
            wraith_walk = scale_animation(wraith_walk, 0.175)
            golem_walk = scale_animation(golem_walk, 0.175)
            satyr_walk = scale_animation(satyr_walk, 0.15)
            # attack = scale_animation(attack)
            # death = scale_animation(death)

            # add animation
            Config().ANIMATIONS['Goblin.walk'] = convert_animation(goblin_walk)
            Config().ANIMATIONS['Wraith.walk'] = convert_animation(wraith_walk)
            Config().ANIMATIONS['Golem.walk'] = convert_animation(golem_walk)
            Config().ANIMATIONS['Satyr.walk'] = convert_animation(satyr_walk)
            # Config().ANIMATIONS['Wraith.attack'] = convert_animation(attack)
            # Config().ANIMATIONS['Wraith.death'] = convert_animation(death)
                
        load_static_images()
        populate_animations()
    
    @staticmethod
    def get_image(image_name):
        if image_name in Config().IMAGES: return Config().IMAGES[image_name]
        else: print(f'WARNING - {image_name = } does not exist!')
        
    @staticmethod
    def get_animation(animation):
        if animation in Config().ANIMATIONS: return Config().ANIMATIONS[animation]
        else: print(f'WARNING - {animation = } does not exist!')
