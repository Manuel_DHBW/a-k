"""
    :Main work Arlt Robin & Klaiber Manuel:
"""

# Imports

import os, sys

# Appriciate the pygame work, but will credit it other ways.
os.environ['PYGAME_HIDE_SUPPORT_PROMPT'] = "hide"  # Hides pygame console message

import pygame
from Button import Button
from Config import Config
from Hud import Hud
from Minion import *
from Player import Player
from Tower import Tower


class TD:
    """Arlt & KLaiber's. tower defence game."""

    # static TD.Variables
    # use case: access & reasigned anywhere

    mouse_down = False
    game_over = False
    minion_counter = []
    towers = []
    enemies = []
    wave_index = 0
    wave_process = False
    wave_startable = True
    tower_placement = False
    current_level = Config().get_level()

    @staticmethod
    def start():
        """Initialises mandatory game variables and starts the game loop."""

        # function scoped variables
        # use cases:
        # - read only values
        # - mutated elements (e.g. list)

        spawn_minion_event = pygame.USEREVENT + 1
        button_colour_toggle_event = pygame.USEREVENT + 2

        buttons = [
            Button(440, 'build tower'),
            Button(480, 'next wave'),
            Button(520, 'restart game'),
            Button(560, 'exit game'),
        ]

        def init():
            """Initialise pygame and set display title."""

            pygame.init()
            pygame.display.set_caption(Config().TITLE)

        def get_screen():
            """
                Initialise and return screen.
                :return pygame.Surface:
            """

            return pygame.display.set_mode(Config().DIMENSIONS)

        def get_surface():
            """
                Initialise, convert and return drawing surface.
                :return pygame.Surface:
            """

            surface = pygame.Surface(screen.get_size())
            return surface.convert()

        def draw_level():
            """Draw level based of 2D list `level` with colour interpretation from `SPRITES`."""

            scale = Config().TILE_SIZE
            level = Config().LEVEL1
            sprites = Config().DEBUG_SPRITES
            width = Config().HIGHLIGHT_BORDER_WIDTH
            
            for y, row in enumerate(level):
                for x, column in enumerate(row):
                    colour = sprites[column] if column in sprites else sprites[-1]
                    tile = pygame.Rect(
                        int(x * scale + width),
                        int(y * scale + width),
                        int(scale - 2 * width),
                        int(scale - 2 * width)
                    )
                    pygame.draw.rect(surface, colour, tile, int(2 * width))


        # Returns the (x, y) coordiante of the tile the mouse is hovering.
        get_mouse_tile_pos = lambda: [pos // Config().TILE_SIZE for pos in pygame.mouse.get_pos()]

        def highlight_hovered_tile():
            """Highlight tile under mouse."""

            x, y = get_mouse_tile_pos()
            scale = Config().TILE_SIZE
            colour_hover = Config().COLOUR_HOVER
            colour_click = Config().COLOUR_CLICK
            colour = colour_click if TD.mouse_down else colour_hover

            tile = pygame.Rect(x * scale, y * scale, scale, scale)
            pygame.draw.rect(surface, colour, tile, int(Config().HIGHLIGHT_BORDER_WIDTH))

        def preview_tower_placement():
            """Preview tower at cursor position."""
            
            if player.get_building_currency() < Tower.cost:
                TD.tower_placement = False
                return
            
            if TD.tower_placement:
                x, y = get_mouse_tile_pos()
                preview_tower = Tower(x, y)
                preview_tower.preview(surface)

        def draw():
            """Call all draw processes and apply them."""

            surface.blit(level_1_map, (0, 0))
            player.draw(surface)
            if Config().DEBUG: draw_level()
            if Config().DEBUG: highlight_hovered_tile()
            preview_tower_placement()
            
            for tower in TD.towers: tower.draw(surface)
            for enemy in TD.enemies[::-1]: enemy.draw_minion(surface)

            hud.draw(surface, player.get_health(), player.get_building_currency())
            for button in buttons: button.draw(surface)
            game_over()

            screen.blit(surface, (0, 0))
            pygame.display.flip()

        def build_tower():
            """Instantaneously build a tower on the hovered tile if possible according to `level`."""

            x, y = get_mouse_tile_pos()
            level = TD.current_level
            
            if level[y][x] != 2 or player.building_currency < 100: return

            player.building_currency -= Tower.cost
            print(f'Player money: {player.building_currency}')
            level[y][x] = 3
            TD.towers.append(Tower(x, y))

        def update():
            """Call all update processes."""
            
            for enemy in TD.enemies:
                if enemy.update(player): TD.enemies.remove(enemy)
                
            TD.enemies.sort(reverse = True, key = lambda enemy: enemy.progress)
            for tower in TD.towers: tower.update(TD.enemies)

        def spawn_wave():
            """Initiates the wave process and resets the `minion_counter`."""
            
            if len(TD.enemies) == 0 and TD.wave_startable:
                TD.minion_counter = []
                waves = Config().WAVES_LEVEL1
                for _ in waves[TD.wave_index]:
                    TD.minion_counter.append(0)
                TD.wave_process = True

        def spawn_minion():
            """Logic for delayed minion spawn."""
            
            path = Config().PATH_LEVEL1
            spawn_list = [
                Minion(path),
                Flying(path),
                Brute(path),
                Runner(path),
            ]
            
            if TD.wave_process:
                waves = Config().WAVES_LEVEL1
                if TD.minion_counter[0] < waves[TD.wave_index][0]:
                    TD.enemies.append(spawn_list[0])
                    TD.minion_counter[0] += 1
                elif TD.minion_counter[1] < waves[TD.wave_index][1]:
                    TD.enemies.append(spawn_list[1])
                    TD.minion_counter[1] += 1
                elif TD.minion_counter[2] < waves[TD.wave_index][2]:
                    TD.enemies.append(spawn_list[2])
                    TD.minion_counter[2] += 1
                elif TD.minion_counter[3] < waves[TD.wave_index][3]:
                    TD.enemies.append(spawn_list[3])
                    TD.minion_counter[3] += 1
                elif sum(TD.minion_counter) == sum(waves[TD.wave_index]):
                    if TD.wave_index < len(waves) - 1:
                        player.increase_building_currency(50 + 15 * TD.wave_index)
                        TD.wave_index += 1
                    TD.wave_process = False

        def toggle_tower_placement():
            TD.tower_placement = not TD.tower_placement

        def game_over():
            """Game over logic."""

            if player.get_health() <= 0:
                width, height = Config().DIMENSIONS
                TD.game_over = True
                font_game_over = pygame.font.Font(None, 144)
                game_over_text = font_game_over.render("Game Over!", True, (180, 0, 0))
                surface.blit(
                    game_over_text,
                    (
                        (width - game_over_text.get_width()) // 2,
                        (height - game_over_text.get_height()) // 2
                    )
                )

        def restart_game():
            """Resets mandatory variables for a smooth restart."""
            
            TD.game_over = False
            TD.minion_counter = []
            TD.wave_index = 0
            TD.wave_process = False
            TD.wave_startable = True
            TD.tower_placement = False
            player.reset()
            TD.enemies = []
            TD.towers = []
            TD.current_level = Config().get_level()

        def save_exit():
            """End pygame and program super save."""

            pygame.quit()
            sys.exit()

        button_functions = {
            'next wave': spawn_wave,
            'exit game': save_exit,
            'restart game': restart_game,
            'build tower': toggle_tower_placement,
        }

        def handleInput():
            """Define response to user input."""

            for event in pygame.event.get():
                if event.type == pygame.QUIT: save_exit()

                # check mouse events
                if (TD.mouse_down):
                    if event.type == pygame.MOUSEBUTTONUP: TD.mouse_down = False
                else:
                    if event.type == pygame.MOUSEBUTTONDOWN:
                        TD.mouse_down = True

                        if event.button == 1:
                            for button in buttons:
                                if button.got_clicked(surface, pygame.mouse.get_pos()):
                                    button_functions[button.get_text()]()
                            if TD.tower_placement:
                                build_tower()
                        if event.button == 3 and TD.tower_placement:
                            TD.tower_placement = False

                # check keyboard events
                # (Digits: K_0 - K_9)
                # (Letters: K_a - K_z)
                # (Specials_ K_<PLUS|MINUS>, K_<UP|LEFT|DOWN|RIGHT>, K_SPACE, K_TAB, K_ESCAPE, K_RETURN(=> enter))
                # (See more: '> help(pygame)')
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE: save_exit()

                    if event.key == pygame.K_RETURN:
                        spawn_wave()
                    elif event.key == pygame.K_SPACE and Config().DEBUG:
                        print(f'Kill {len(TD.enemies)} minions')
                        for x in TD.enemies:
                            x.minion_got_hit(9999)  # jrpg feelings, sry^^
                    elif event.key == pygame.K_TAB and Config().DEBUG:
                        player.building_currency += 500;
                        print(f'Cheat money: {player.building_currency} $$$');
                    elif event.key == pygame.K_t:  # build_tower()
                        TD.tower_placement = not TD.tower_placement

                # custom Event
                elif event.type == spawn_minion_event:
                    spawn_minion()

                elif event.type == button_colour_toggle_event:
                    """Custom Event with logic click ability and with colour indication."""

                    for button in buttons[1:]: button.reset_colour(surface)

                    if TD.game_over:
                        TD.wave_startable = False
                        buttons[1].change_colour(surface, (180, 0, 0))
                    elif TD.wave_process:
                        TD.wave_startable = False
                        buttons[1].change_colour(surface, (100, 100, 100))
                    elif len(TD.enemies) == 0:
                        TD.wave_startable = True
                        buttons[1].reset_colour(surface)

        def game_loop():
            """Call core application methods."""

            while True:
                clock.tick(Config().FPS)
                
                draw()
                handleInput()
                update()


        # start body

        print('Welcome to A&K\'s tower defense')
        print('The "super amazing luxury annihilation defender"', end = '\n' * 2)

        # preparations
        init()
        screen = get_screen()
        surface = get_surface()
        
        Config().setup()
        player = Player()
        hud = Hud()
        clock = pygame.time.Clock()
        
        level_1_map = Config().get_image('Level1')

        try:
            pygame.mixer.music.load('assets/music/Chiptune_Nobility.mp3')
            pygame.mixer.music.set_volume(0.1)
            pygame.mixer.music.play(-1)
        except pygame.error:
            print('Looks like the music cannot be played - what a pity.')

        # CUSTOM EVENTS - runs event every 1000ms; command to only run once with pygame 2.0
        pygame.time.set_timer(spawn_minion_event, Config().SPAWN_SPEED_MS)
        pygame.time.set_timer(button_colour_toggle_event, 100)

        game_loop()


if __name__ == '__main__':

    TD.start()
