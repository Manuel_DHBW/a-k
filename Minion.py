"""
    :Main work Arlt Robin:
    :Supported Klaiber Manuel:
"""

import pygame
import math
from Config import Config

class Minion:
    """
        Minion moving towards the players base. If not killed prior it will deal damage to the player.
    """

    STEP_PROGRESS = 1 / 2017 # fraction of 
    ANIMATION_FRAME_DURATION_MS = 150
    ANIMATION = 'Goblin.walk'
    Y_OFFSET_FAKTOR = 0.7
    

    def set_sprite_time(self):
        """Calculate and save the next time the animation should be changed again."""
        
        self.time_next_sprite = pygame.time.get_ticks() + Minion.ANIMATION_FRAME_DURATION_MS

    def set_hitbox(self):
        """Update position of the hitbox to the current location of the Minion."""
        
        self.hitbox = self.rect = pygame.Rect(
            self.pos_x - 0.5 * self.width,
            self.pos_y - self.Y_OFFSET_FAKTOR * self.height,
            self.width,
            self.height
        )

    def __init__(self, given_path):
        self.animation = Config().get_animation(self.ANIMATION)
        width, height = self.animation[0].get_rect()[-2:] # all sprites have the same rect
        self.curent_sprite = 0
        self.width = width
        self.height = height
        
        self.move_index = 0
        self.path = given_path
        self.pos_x = self.path[0][0]
        self.pos_y = self.path[0][1]
        self.speed = 2
        self.minion_health = 5
        self.step_counter = 0
        self.damage_to_player = 1
        self.currency_generation = 10
        self.progress = 0
        
        self.set_sprite_time()
        self.set_hitbox()

    def draw_minion(self, surface):
        """Draws the Minion. If DEBUG is set additionaly the the hitbox will be drawn."""
        
        surface.blit(self.animation[self.curent_sprite], self.hitbox[:2])
        if Config().DEBUG: pygame.draw.rect(surface, Config().COLOUR_HITBOX, self.hitbox, 2)

    def minion_got_hit(self, damage):
        """Reduces minion hp after hit."""
        
        self.minion_health -= damage

    def minion_death(self, given_player):
        """Checks and handles action on minion death (killed or finished path)."""
        
        # minon killed
        if self.minion_health <= 0:
            given_player.increase_building_currency(self.get_currency_generation())
            print(f'Gained {self.get_currency_generation()} gold')
            return True
        
        # minion finished path
        x, y = self.path[len(self.path) - 1]
        if (self.pos_x == x) and (self.pos_y == y):
            given_player.decrease_player_health(self.damage_to_player)
            return True

    def get_currency_generation(self):
        """Returns the value of currency to be generated after defeat of minion."""
        
        return self.currency_generation

    def update_curent_sprite(self):
        """
            Select next sprite after defined frame duration.
            Reset Animation should the last frame be reached.
        """
        
        if self.time_next_sprite < pygame.time.get_ticks():
            self.curent_sprite = (self.curent_sprite + 1) % (len(self.animation) - 1)
            self.set_sprite_time()

    def set_position(self, new_x, new_y):
        """Function to update minion position."""
        
        self.pos_x = new_x
        self.pos_y = new_y

    # TODO refactor as to have the path as argument
    def move_target_coordinates(self):
        """
            Calculates distance between coordinates given as list.
            Moves minion along shortest path with `self.speed`.
        """
        
        if self.move_index == 0:
            self.set_position(self.path[0][0], self.path[0][1])
        if self.move_index < len(self.path):
            target_x = self.path[self.move_index][0]
            target_y = self.path[self.move_index][1]
            distance_x = target_x - self.pos_x
            distance_y = target_y - self.pos_y
            math_sign_x = 1
            math_sign_y = 1
            if distance_x < 0:
                math_sign_x = -1
            if distance_y < 0:
                math_sign_y = -1

            distance = math.sqrt(distance_x ** 2 + distance_y ** 2)
            if distance > self.speed:
                distance_proportion_x = distance_x ** 2 / distance ** 2
                distance_proportion_y = distance_y ** 2 / distance ** 2
                move_x = (distance_proportion_x * self.speed) * math_sign_x + self.pos_x
                move_y = (distance_proportion_y * self.speed) * math_sign_y + self.pos_y
                self.set_position(move_x, move_y)
                self.step_counter += 1
            else:
                self.set_position(target_x, target_y)
                self.move_index += 1
                self.step_counter += 1

        elif self.move_index == len(self.path):
            self.move_index = 0

    def update(self, given_player):
        """
            Update animation, position, progress and hitbox.
            :return bool: True if the minion is dead.
        """
        
        self.update_curent_sprite()
        self.move_target_coordinates()
        self.progress += self.STEP_PROGRESS * self.speed
        self.set_hitbox()
        return self.minion_death(given_player)


class Flying(Minion):
    """
        Flying variation of the minion - flyes above the ground, henec it will aim
        towards the players base in a straight line.
    """
    
    STEP_PROGRESS = 1 / 1201
    ANIMATION = 'Wraith.walk'
    Y_OFFSET_FAKTOR = 0.5

    def __init__(self, given_path):
        super(Flying, self).__init__(given_path)
        self.path = self.path[::len(self.path) - 1]


class Brute(Minion):
    """Thoughest variation of the minions. They withstand significantly more shots before they die."""
    
    ANIMATION = 'Golem.walk'
    Y_OFFSET_FAKTOR = 0.8
    
    def __init__(self, given_path):
        super(Brute, self).__init__(given_path)
        self.minion_health = 10


class Runner(Minion):
    """Runners are good at their job and they love doing it. Constantly fast minion variation."""
    
    ANIMATION = 'Satyr.walk'
    Y_OFFSET_FAKTOR = 0.65
    
    def __init__(self, given_path):
        super(Runner, self).__init__(given_path)
        self.speed = self.speed * 1.5

