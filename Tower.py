"""
    :Main work Klaiber Manuel:
    :Supported Arlt Robin:
"""

import pygame
from Config import Config

class Tower:
    """
        The Tower will shoot a nearby unit thus damaging it.
        Once shot the tower will not be able to shot again for a short duration.
    """

    cost = 100  # building cost
    DAMAGE = 1  # base damage 
    range = 3  # radius in tiles
    FIRE_RATE = 5  # shots / second
    SHOT_DELAY_MS = 1000 / FIRE_RATE
    IMG = pygame.image.load('assets/towers/Tier1_tower_v1.png')


    def set_shot_time(self):
        """Calculate and save the next time the tower may shot again."""
        
        self.time_next_shot = pygame.time.get_ticks() + Tower.SHOT_DELAY_MS

    def __init__(self, x :int, y :int):
        """Place a tower at the corresponding position on the Level."""
        
        self.x = x
        self.y = y
        
        radius = Tower.range * 0.7
        scale = Config().TILE_SIZE
        padding = 5
        
        self.scaled_range = Tower.range * scale
        self.body = ((x - 0.05) * scale + padding, (y - 0.75) * scale + padding)
        self.center = (int((x + 0.5) * scale), int((y + 0.5) * scale))
        self.rect = pygame.Rect(
            (0.5 + (x - radius)) * scale,
            (0.5 + (y - radius)) * scale,
            radius * scale * 2,
            radius * scale * 2
        )
        
        self.set_shot_time()
    
    def draw_radius(self, surface):
        """Draws an indication of the towers range."""
        
        pygame.draw.circle(
            surface,
            Config().COLOUR_SHOT_RANGE,
            self.center,
            self.scaled_range,
            int(Config().HIGHLIGHT_BORDER_WIDTH)
        )
    
    def draw(self, surface):
        """Draw the tower and range indication."""
        
        surface.blit(Tower.IMG, self.body)
        self.draw_radius(surface)

    def preview(self, surface):
        """Draw a transparent preview of a tower with ranfge indication."""
        
        preview_tower = Tower.IMG.copy()
        preview_tower.set_alpha(150)

        surface.blit(preview_tower, self.body)
        self.draw_radius(surface)

    def can_fire(self):
        """
            :return bool: True if the Tower is allowed to shot else False.
        """
        
        return pygame.time.get_ticks() > self.time_next_shot
    
    def get_targets_in_range(self, targets :(list, tuple)):
        """
            Each target is checked for collision with the Towers `rect` hitbox
            and added to the output list if they do. 
            :return list: Targets in range of the Tower.
        """
        
        return [target for target in targets if pygame.sprite.collide_circle(self, target)]
    
    def select_targets(self, targets :(list, tuple)):
        """Selection process to identify which targets should be attacked by the tower."""
        
        return targets[:1]
    
    def attack(self, targets :(list, tuple)):
        """Shots targets and sets the shot timer."""
        
        for target in targets: target.minion_got_hit(Tower.DAMAGE)
        self.time_next_shot = pygame.time.get_ticks() + Tower.SHOT_DELAY_MS
    
    def update(self, targets :(list, tuple)):
        """
            If the tower can currently shot it will go through the target selection progress.
            Remaining Targets will be attacked.
            :param targets: (list, tuple) of Minions.
            
        """
        
        if not self.can_fire(): return
        targets = self.get_targets_in_range(targets)
        targets = self.select_targets(targets)
        if targets: self.attack(targets)
