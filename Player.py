"""
    :Main work Arlt Robin:
    :Supported Klaiber Manuel:
"""

import pygame
from Config import Config

class Player:
    """Responsible for maintaining player resources."""

    BASE = pygame.image.load('assets/bases/Level1_Fortress.png')

    def __init__(self):
        self.health = 10
        self.building_currency = 100
        self.spell_currency = 50

    def increase_building_currency(self, increase):
        self.building_currency += increase

    def decrease_building_currency(self, cost):
        self.building_currency -= cost

    def decrease_player_health(self, damage):
        self.health -= damage
        if self.health < 0: self.health = 0

    def get_health(self):
        return self.health

    def get_building_currency(self):
        return self.building_currency

    def get_spell_currency(self):
        return self.spell_currency
    
    def draw(self, surface):
        x, y = Config().PATH_LEVEL1[-1]
        surface.blit(Player.BASE, (x - 15, y - 95))

    def reset(self):
        self.__init__()

