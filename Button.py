"""
    :Mainwork Arlt Robin:
"""


import pygame


class Button:

    def __init__(self, y, text, colour = (160, 180, 160)):
        self.x = 855
        self.y = y
        self.width = 100
        self.height = 30
        self.text = text
        self.colour = colour
        self.default_colour = colour

    def draw(self, surface):
        font = pygame.font.Font(None, 20)
        button_text = font.render(self.text, True, (0, 0, 0))
        pygame.draw.rect(surface, (0, 0, 0), (self.x - 2, self.y - 2, self.width + 4, self.height + 4))
        pygame.draw.rect(surface, self.colour, (self.x, self.y, self.width, self.height))
        surface.blit(button_text, (
            self.x + ((self.width - button_text.get_width()) / 2),
            self.y + ((self.height - button_text.get_height()) / 2))
        )

    # deprecated
    def get_position(self):
        return self.x, self.y

    def get_button_endpoint(self):
        return self.x + self.width + 4, self.y + self.height + 4

    def get_button_startpoint(self):
        return self.x - 2, self.y - 2

    def change_colour(self, surface, new_colour):
        self.colour = new_colour
        self.draw(surface)

    def reset_colour(self, surface):
        self.colour = self.default_colour
        self.draw(surface)

    def got_clicked(self, surface, position_cursor):
        """Checks whether button position and cursor position are equal, if so returns True"""
        x, y = position_cursor
        start_x, start_y = self.get_button_startpoint()
        end_x, end_y = self.get_button_endpoint()

        if start_x <= x <= end_x and start_y <= y <= end_y:
            self.change_colour(surface, (230, 230, 230))
            return True

    def get_text(self):
        return self.text
